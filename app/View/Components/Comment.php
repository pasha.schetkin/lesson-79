<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;
use App\Models\Comment as CommentObject;

class Comment extends Component
{
    public CommentObject $comment;

    /**
     * Create a new component instance.
     *
     * @param CommentObject $comment
     */
    public function __construct(CommentObject $comment)
    {
        $this->comment = $comment;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render(): string|View
    {
        return view('components.comment', $this->comment);
    }
}
