<?php

namespace App\Policies;

use App\Models\Article;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user): bool
    {
        if ($user) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        return Auth::check();
    }

    /**
     * @param User $user
     * @param Article $article
     * @return bool
     */
    public function update(User $user, Article $article): bool
    {
        if ($user->is_admin) {
            return true;
        }
        return $user->id == $article->user_id;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Article $article
     * @return mixed
     */
    public function delete(User $user, Article $article): bool
    {
        if ($user->is_admin) {
            return true;
        }
        return $user->id == $article->user_id;
    }

    /**
     * @param User $user
     * @param Article $article
     * @return bool
     */
    public function edit(User $user, Article $article): bool
    {
        return $user->id == $article->user_id;
    }
}
