<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Comment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;

class CommentController extends Controller
{
    /**
     * CommentsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @param Article $article
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(Request $request, Article $article): JsonResponse
    {
        $request->validate([
            'body' => 'required|min:5'
        ]);

        $comment = new Comment();
        $comment->user_id = $request->user()->id;
        $comment->body = $request->input('body');
        $comment->article_id = $article->id;
        $comment->save();

        return response()->json([
            'comment' => view('components.comment', compact('comment'))->render()
        ], 201);
    }
}
