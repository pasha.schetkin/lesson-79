<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleRequest;
use App\Models\Article;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Redirector;

class ArticleController extends Controller
{
    /**
     * ArticlesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        $articles = Article::paginate(8);
        return view('articles.index', compact('articles'));
    }

    /**
     * Display the specified resource.
     *
     * @param Article $article
     * @return Application|Factory|View|Response
     */
    public function show(Article $article)
    {
        $article->setRelation('comments', $article->comments()->paginate(1));
        return view('articles.show', compact('article'));
    }

    /**
     * @return Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('articles.create');
    }

    /**
     * @param ArticleRequest $request
     * @return RedirectResponse|Redirector
     */
    public function store(ArticleRequest $request)
    {
        $article = new Article();
        $article->title = $request->input('title');
        $article->content = $request->input('content');
        $article->user_id = $request->user()->id;
        $article->save();

        return redirect(route('dashboard'));
    }

    /**
     * @param Article $article
     * @return Factory|\Illuminate\View\View
     * @throws AuthorizationException
     */
    public function edit(Article $article)
    {
        $this->authorize('update', $article);
        return view('articles.edit', compact('article'));
    }

    /**
     * @param ArticleRequest $request
     * @param Article $article
     * @return RedirectResponse|Redirector
     */
    public function update(ArticleRequest $request, Article $article)
    {
        $article->update($request->all());
        return redirect(route('articles.index'));
    }

    /**
     * @param Article $article
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function destroy(Article $article): JsonResponse
    {
        $this->authorize('delete', $article);
        $article->delete();

        return response()->json([
            'status'=> 'ok'
        ], 204);
    }
}
