#!/usr/bin/env bash
# shellcheck disable=SC2086
set -e

# IP or hostname, example: root@82.196.9.144 or root@example.com
HOST=${1}

# production, demo or staging etc...
INSTANCE_NAME=${2}

# Stable version
VERSION=${3}

PATH_TO_SCRIPTS_DIR=${4}/scripts
PATH_TO_NGINX_CONFIG_FILE=${PATH_TO_SCRIPTS_DIR}/nginx/default.conf
APPLICATION_ROOT_PATH=/home/demo

# Archive name
LATEST_ARTIFACT_NAME=application-${VERSION}.tar.gz

# Path to archive
LATEST_ARTIFACT_PATH=${HOME}/artifacts/${LATEST_ARTIFACT_NAME}

info() {
echo -e "\e[33m[Info]     \e[33m$1 \e[39m $(for i in {12..21} ; do echo -en "\e[33;1;${i}m>\e[0m" ; done ; echo)"
}

success() {
echo -e "\e[32m[Success] \e[32m $1 \e[39m $(for i in {12..21} ; do echo -en "\e[32;1;${i}m>\e[0m" ; done ; echo)"
}

script_execution_dir=$(pwd)
info "Scripts run under ${script_execution_dir} directory"

scp ${PATH_TO_SCRIPTS_DIR}/apply-update.sh ${HOST}:${APPLICATION_ROOT_PATH}
scp ${PATH_TO_NGINX_CONFIG_FILE} ${HOST}:/etc/nginx/sites-available/default
scp ${LATEST_ARTIFACT_PATH} ${HOST}:${APPLICATION_ROOT_PATH}

echo ${VERSION} > VERSION
scp VERSION ${HOST}:${APPLICATION_ROOT_PATH}

# shellcheck disable=SC2087
ssh ${HOST} <<EOL
chmod +x apply-update.sh
${APPLICATION_ROOT_PATH}/apply-update.sh ${INSTANCE_NAME} ${LATEST_ARTIFACT_NAME} ${APPLICATION_ROOT_PATH}
EOL
