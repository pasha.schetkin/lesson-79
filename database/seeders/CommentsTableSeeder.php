<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Comment;
use App\Models\User;
use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $articles = Article::all();
        $users = User::all();
        $articles->each(function($article) use ($users) {
           $user = $users->whereNotIn('id', $article->user->id)->random();
           Comment::factory()->count(rand(3, 6))->for($user)->for($article)->create();
        });
    }
}
