@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-6">
            <div class="row">
                <div class="col-2">
                    <h3>Articles</h3>
                </div>
                @if (Auth::check())
                    <div class="col-4">
                        <a href="{{route('articles.create')}}" class="btn btn-primary btn-sm
  active" role="button" aria-pressed="true">Create new Article</a>
                    </div>
                @endif
            </div>
        </div>
    </div>

  <div class="row">
    @csrf
    @foreach($articles as $article)
      <div class="col" style="padding: 35px 0 0 0;" id="delete-article-{{$article->id}}">
        <div class="card" style="width: 18rem;">
          <div class="card-header">
            {{ $article->title }}
            <span class="badge badge-info" style="cursor: pointer"
                  title="Comments count">
{{$article->comments->count()}}
</span>
            @can('delete', $article)
              <span data-article-id="{{$article->id}}" class="delete" aria-hidden="true">&times;</span>
            @endcan
          </div>
          <div class="card-body">
            <blockquote class="blockquote mb-0">
              <p class="text-truncate">
                {{ $article->content }}
              </p>
            </blockquote>
            <a href="{{route('articles.show', ['article' =>
$article])}}" class="card-link">Show more ...</a> <br>
            @can('update', $article)
              <a href="{{route('articles.edit', ['article' =>
$article])}}" class="card-link">Edit</a>
            @endcan
          </div>
        </div>
      </div>
    @endforeach
  </div>
  <div class="row p-5">
    <div class="col offset-4">
      {{ $articles->links('pagination::bootstrap-4') }}
    </div>
  </div>
@endsection
