$(document).ready(function () {
  $('.delete').on('click', function (event) {
    const articleId = $(this).attr('data-article-id');
    const token = $('input[type=hidden]').val();
    const article = $('#delete-article-' + articleId);
    console.log('ARTICLE ====> ', article);
    $.ajax({
      url: `articles/${articleId}`,
      method: 'delete',
      data: {_token: token}
    })
      .done(function(response) {
        console.log('response => ', response);
        $(article).remove();
      })
      .fail(function(response) {
        console.log('fail response => ', response);
      });
  })
});