<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    use RefreshDatabase;
    private Model|Collection $user;
    private Model|Collection $articles;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->articles = Article::factory()->for($this->user)->count(8)->create();
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function test_success_get_articles()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSeeText("Articles");
        $response->assertSeeText("Login");
        $response->assertSeeText("Register");
        $response->assertSeeText(config('app.name'));
        $response->assertViewHas('articles');

        foreach ($this->articles as $article) {
            $response->assertSeeText($article->title);
        }
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function test_redirect_if_user_not_auth()
    {
        $response = $this->get(
            route('articles.show', [
                'article' => $this->articles->first()
            ])
        );
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function test_forbidden()
    {
        $another_user = User::factory()->create();
        $this->actingAs($another_user);
        $response = $this->delete(
            route(
                'articles.destroy', ['article' => $this->articles->first()]
            )
        );

        $response->assertForbidden();
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function test_success_get_article_create_form()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('articles.create'));
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function test_failed_get_article_create_form()
    {
        $response = $this->get(route('articles.create'));
        $response->assertStatus(302);
        $response->assertRedirect(route('login'));
    }

    /**
     * A basic feature test example.
     * @group articles
     * @return void
     */
    public function test_success_create_article()
    {
        $this->actingAs($this->user);
        $data = [
            'title' => 'Test title',
            'content' => 'Test content'
        ];

        $response = $this->post(route('articles.store'), $data);
        $response->assertStatus(302);
        $this->assertDatabaseHas('articles', $data);
    }
}
