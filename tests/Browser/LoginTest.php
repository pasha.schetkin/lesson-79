<?php

namespace Tests\Browser;

use App\Models\User;
use Tests\DuskTestCase;
use Throwable;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group test2
     * @return void
     * @throws Throwable
     */
    public function testEmailValidation()
    {
        $user = User::factory()->state([
            'email' => 'lesson@example.com'
        ])->create();
        $this->browse(function ($browser) use ($user) {
            $browser->visit('/login')
                ->type('email', 'WRONG@example.com')
                ->type('password', 'password')
                ->press('Login')
                ->assertPathIsNot('/articles')
                ->assertSee('These credentials do not match our records.');
        });
    }

    /**
     * A Dusk test example.
     * @group test
     * @return void
     * @throws Throwable
     */
    public function testSuccessLogin()
    {
        $user = User::factory()->state([
            'email' => 'lesson@example.com'
        ])->create();

        $this->browse(function ($browser) use ($user) {
            $browser->visit('/login')
                ->type('email', $user->email)
                ->type('password', 'password')
                ->press('Login')
                ->assertPathIs('/articles');
        });
    }


}
