# Jenkins server
## Установка пхп
	1. sudo add-apt-repository ppa:ondrej/php
	2. sudo apt-get update
	3. sudo apt-get install php8.0 php8.0-curl php8.0-mbstring php-sqlite3 php-xml php8.0-zip php-mbstring php-xml php8.0-fpm

## Установка стабильных зависимостей для фронтенда
	4. sudo apt install nodejs
	5. sudo apt install npm
	6. sudo npm install -g n
	7. sudo n stable

## Установка стабильного хрома для запусков тестов
	8. wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
	9. sudo apt install ./google-chrome-stable_current_amd64.deb

# Демонстрационный сервер
## Установка пхп
	1. sudo add-apt-repository ppa:ondrej/php
	2. sudo apt-get update
	3. sudo apt-get install php8.0 php8.0-curl php8.0-mbstring php-sqlite3 php-xml php8.0-zip php-mbstring php-xml php8.0-fpm

## Установка nginx
	4. sudo apt update
	5. sudo apt install nginx

## Затем настраиваем nginx

Пример конфига лежит в директории `scripts/nginx/default.conf`

# Docker
## Установка Docker

#### Установка необходимых зависимостей на "машину"
    1. sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
    2. curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    3. sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"
    4. sudo apt-get update
    5. sudo apt-get install docker-ce
#### Затем нужно дать авторизованному пользователю доступ к группе которая работает с docker
```bash
sudo usermod -a -G docker {{ваш логин}}
```
Псле этого нужно перезагрузить сессию авторизации в системе чтобы назначение группы применилось. На локальном компьютере вы просто делаете Log out из системы и заходите снова.

Чтобы проверить, работает ли Docker нужно в терминале ввести команду:
```bash
docker ps
```
Вы должны увидеть что-то подобное:
![img.png](readme/docker_ps.png)


